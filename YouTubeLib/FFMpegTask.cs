﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTubeLib
{
    /// -------------------------------------------------------------------------------------------------
    /// <summary>   Values that represent fmpeg tasks. </summary>
    internal enum FFmpegTask
    {
        /// <summary>   An enum constant representing the convert option. </summary>
        Convert,

        /// <summary>   An enum constant representing the get meta data option. </summary>
        GetMetaData,

        /// <summary>   An enum constant representing the get thumbnail option. </summary>
        GetThumbnail
    }
}
