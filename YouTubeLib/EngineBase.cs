﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace YouTubeLib
{
    public class EngineBase : IDisposable
    {
        /// <summary>   Used for locking the FFmpeg process to one thread. </summary>
        private const string LockName = "YouTubeLib.Engine.LockName";

        /// <summary>   The Mutex. </summary>
        protected readonly Mutex Mutex;

        /// <summary>   The ffmpeg process. </summary>
        protected Process FFmpegProcess;

        /// <summary>   Full pathname of the FFmpeg file. </summary>
        protected readonly string FFmpegFilePath;

        protected EngineBase(string ffMpegPath)
        {
            this.Mutex = new Mutex(false, LockName);
            if (File.Exists(ffMpegPath))
                this.FFmpegFilePath = ffMpegPath;
            else this.FFmpegFilePath = Globals.FFMpegPath;
            EnsureFFmpegIsNotUsed();
        }
        private void EnsureFFmpegIsNotUsed()
        {
            try
            {
                this.Mutex.WaitOne();
                Process[] processes = Process.GetProcessesByName(FFmpegFilePath.Split('\\').Last().Split('.').First());
                foreach (Process p in processes)
                {
                    p.Kill();
                    p.WaitForExit();
                }
            }
            catch { }
            finally
            {
                this.Mutex.ReleaseMutex();
            }
        }
        public void Dispose()
        {
            if (FFmpegProcess != null)
            {
                this.FFmpegProcess.Dispose();
            }
            this.FFmpegProcess = null;
        }
    }
}
