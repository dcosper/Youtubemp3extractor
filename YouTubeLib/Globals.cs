﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace YouTubeLib
{
    public static class Globals
    {
        public static string AppPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        public static string MP3Path = Path.Combine(AppPath, "Mp3");
        public static string FFMpegPath = Path.Combine(AppPath, "FFMpeg.exe");

        public static string StringBetween(string prefix, string suffix, string parent)
        {
            int start = parent.IndexOf(prefix) + prefix.Length;

            if (start < prefix.Length)
                return string.Empty;

            int end = parent.IndexOf(suffix, start);

            if (end == -1)
                end = parent.Length;

            return parent.Substring(start, end - start);
        }
        public static int SkipWhitespace(this string text, int start)
        {
            int result = start;
            while (char.IsWhiteSpace(text[result]))
                result++;
            return result;
        }
        public static void EnsureDirectories()
        {
            if (!Directory.Exists(MP3Path))
                Directory.CreateDirectory(MP3Path);
        }
        public static string RemoveIllegalPathCharacters(this string value)
        {
            return value.Replace(' ', '_')
                .Replace('\\', '_')
                .Replace('/', '_')
                .Replace(':', '_')
                .Replace('*', '_')
                .Replace('?', '_')
                .Replace('"', '_')
                .Replace('<', '_')
                .Replace('>', '_')
                .Replace('|', '_');
        }
        public static string GetPathWithoutExtention(this string value)
        {
            return value.Split('.').First();
        }
    }
}
