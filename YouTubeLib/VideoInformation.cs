﻿/* YoutubeSearch
 * It is a library for .NET, written in C#, to show search query results from YouTube.
 *
 * (c) 2016 Torsten Klinger - torsten.klinger(at)googlemail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see<http://www.gnu.org/licenses/>.
 * */
using System.Drawing;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace YouTubeLib
{
    public class VideoInformation
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Duration { get; set; }
        public string Url { get; set; }
        public string Thumbnail { get; set; }

        public async Task<Image> GetThumbnail()
        {
            using (var webClient = new WebClient())
            {
                byte[] data = await webClient.DownloadDataTaskAsync(new System.Uri(Thumbnail));

                using (var mstream = new MemoryStream(data))
                {
                    return Image.FromStream(mstream);
                }
            }
        }
    }
}
