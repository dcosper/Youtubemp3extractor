﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTubeLib.Events
{
    public class Events
    {
        public delegate void OnProgressChanged(int percentage);
        public delegate void OnError(Exception ex);
    }
}
