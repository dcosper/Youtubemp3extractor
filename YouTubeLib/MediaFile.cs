﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTubeLib
{
    public class MediaFile
    {
        public MediaFile() { }

        public MediaFile(string filename)
        {
            Filename = filename;
        }

        public string Filename { get; set; }

        public Metadata Metadata { get; internal set; }

    }
}
