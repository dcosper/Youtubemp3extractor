﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YoutubeExplode;
using YoutubeExplode.Models.MediaStreams;
using YouTubeLib.Events;
using YouTubeLib.Options;
using static YouTubeLib.Events.Events;

namespace YouTubeLib
{
    public class YoutubeConvertor : IDisposable
    {
        public event OnProgressChanged OnProgressUpdate;
        public event OnError OnDownloadError;
        public event OnError OnConversionError;
        public void Dispose()
        {
        }
        /// <summary>
        /// Downloads a link from youtube and saves it to disk
        /// </summary>
        /// <param name="uri">the link</param>
        /// <param name="directoryLocation">the directory to save to, not a filename !</param>
        /// <returns></returns>
        public async Task<string> DownLoadYoutubeLink(string uri, string directoryLocation)
        {
            return await Task.Run(async () =>
            {
                if (!IsValidUri(uri))
                    return string.Empty;
                
                string id = string.Empty;
                YoutubeExplode.Models.Video video = null;
                MuxedStreamInfo streamInfo = null;
                string fileName = string.Empty;
                var client = new YoutubeClient();

                try
                {
                    id = YoutubeClient.ParseVideoId(uri);
                    video = await client.GetVideoAsync(id);
                    var streamInfoSet = await client.GetVideoMediaStreamInfosAsync(id);
                    streamInfo = streamInfoSet.Muxed.WithHighestVideoQuality();
                    var ext = streamInfo.Container.GetFileExtension();
                    fileName = Path.Combine(directoryLocation, video.Title.RemoveIllegalPathCharacters() + "." + ext);
                    var progress = new Progress<double>(p => OnProgressChange((decimal)p));
                    await client.DownloadMediaStreamAsync(streamInfo, fileName, progress);
                }
                catch (Exception ex)
                {
                    ThrowDownloadError(ex);
                }
                return fileName;
            });
        }
        public async Task<string> TryMediaConvert(string inputFile, ConvertableType toType, bool removeVideo = true)
        {
            return await Task.Run(() =>
            {
                if (!File.Exists(inputFile))
                    return string.Empty;
                
                string convertedFilename = string.Format("{0}.{1}", inputFile.GetPathWithoutExtention(), toType.ToString().ToLower());

                var inputVideo = new MediaFile(inputFile);
                var outputMP4 = new MediaFile(convertedFilename);

                
                using (var engine = new Engine())
                {
                    engine.ConversionCompleteEvent += Engine_ConvertProgressEvent;
                    try
                    {
                        engine.Convert(inputVideo, outputMP4);
                    }
                    catch(Exception ex) { ThrowConversionError(ex); return string.Empty; }
                }
                if(removeVideo)
                {
                    try
                    {
                        File.Delete(inputFile);
                    }
                    catch (Exception ex) { ThrowConversionError(ex); }
                }
                return convertedFilename;
            });
        }
        private void Engine_ConvertProgressEvent(object sender, ConversionProgressEventArgs e)
        {
            OnProgressChange(Divide(e.ProcessedDuration, e.TotalDuration));
        }
        private void ThrowDownloadError(Exception ex)
        {
            if (OnDownloadError != null)
                OnDownloadError(ex);
        }
        private void ThrowConversionError(Exception ex)
        {
            if (OnConversionError != null)
                OnConversionError(ex);
        }
        private void OnProgressChange(decimal value)
        {
            if (OnProgressUpdate != null)
                OnProgressUpdate(DecimalToPercent(value));
        }
        private static decimal Divide(TimeSpan dividend, TimeSpan divisor)
        {
            return Math.Floor((decimal)dividend.Ticks) / Math.Floor((decimal)divisor.Ticks);
        }
        public static bool IsValidUri(String uri)
        {
            try
            {
                new Uri(uri);
                return true;
            }
            catch
            {
                return false;
            }
        }
    private int DecimalToPercent(decimal d)
    {
        // we get 0.xx then 1.00 is 100
        if (d >= 1)
            return 100;
        string value = d.ToString("0.00").Substring(2, 2);
        int result = 0;
        if (!int.TryParse(value, out result))
            return -1;
        return result;
    }
}
}
