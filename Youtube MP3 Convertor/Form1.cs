﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Youtube_MP3_Convertor.Controls;
using YouTubeLib;
using YouTubeLib.Options;

namespace Youtube_MP3_Convertor
{
    public partial class Form1 : Form
    {
        public string LastMP3SavePath;
        public static Form1 Instance;
        public Form1()
        {
            InitializeComponent();
            Instance = this;
        }

        private void ResetProgressBar() => progressBar.Value = 0;

        private async void btn_Convert_Click(object sender, EventArgs e)
        {
            bool success = false;
            if (!YoutubeConvertor.IsValidUri(txt_Link.Text))
            {
                MessageBox.Show(string.Format("{0} is a invalid youtube link, try again.", txt_Link.Text), "Invalid Youtube link!!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            FolderBrowserDialog f = new FolderBrowserDialog();
            f.Description = "Select your MP3 directory.";
            if (!string.IsNullOrEmpty(LastMP3SavePath))
            {
                f.SelectedPath = LastMP3SavePath;
                success = await SaveMusic(f.SelectedPath);
                return;
            }

            DialogResult r = f.ShowDialog();

            if (r == DialogResult.OK)
            {
                LastMP3SavePath = f.SelectedPath;
                success = await SaveMusic(f.SelectedPath);
            }
            if (!success)
                MessageBox.Show("Eror saving");
        }
        private async Task<bool> SaveMusic(string path)
        {
            using (YoutubeConvertor youtubeConvertor = new YoutubeConvertor())
            {
                try
                {
                    ResetProgressBar();
                    youtubeConvertor.OnDownloadError += DownLoadError;
                    youtubeConvertor.OnProgressUpdate += DownLoadUpdate;
                    youtubeConvertor.OnConversionError += ConversionError;
                    UpdateStatus(string.Format("Downloading..."));
                    string fileName = await youtubeConvertor.DownLoadYoutubeLink(txt_Link.Text, path);
                    DownLoadUpdate(100);
                    string mp3PreName = string.Format("{0}.{1}", fileName.GetPathWithoutExtention(), ConvertableType.MP3.ToString().ToLower());

                    if (File.Exists(mp3PreName))
                    {
                        DialogResult exists = MessageBox.Show(string.Format("File {0} exists, overwrite it ?", mp3PreName), "Overwite exisiting file ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                        if (exists != DialogResult.Yes)
                            return false;
                    }
                    
                    await Task.Delay(100);

                    ResetProgressBar();
                    UpdateStatus(string.Format("Creating {0}...", mp3PreName));
                    string convertedFilename = await youtubeConvertor.TryMediaConvert(fileName, YouTubeLib.Options.ConvertableType.MP3);
                    UpdateStatus(string.Format("File {0} saved.", convertedFilename));
                    DownLoadUpdate(100);
                    return true;
                }
                catch(Exception ex)
                {
                    ConversionError(ex);
                    return false;
                }
            }
        }
        private void ConversionError(Exception ex) => MessageBox.Show(ex.Message, "Error Converting!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        private void UpdateStatus(string message)
        {
            if (label_Status.InvokeRequired)
            {
                label_Status.Invoke((MethodInvoker)
                    delegate
                    {
                        label_Status.Text = message;
                    });
            }
            else label_Status.Text = message;
        }
        private void DownLoadUpdate(int percentage)
        {
            if (progressBar.InvokeRequired)
            {
                progressBar.Invoke((MethodInvoker)
                    delegate
                    {
                        if (percentage > progressBar.Value)
                            progressBar.Value = percentage;
                    });
            }
            else
            {
                if (percentage > progressBar.Value)
                    progressBar.Value = percentage;
            }
        }
        
        private void DownLoadError(Exception ex) => MessageBox.Show(ex.Message, "Error Downloading!", MessageBoxButtons.OK, MessageBoxIcon.Stop);

        private async void btn_Search_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt_Search.Text))
                return;
            List<VideoInformation> results = await new VideoSearch().SearchQuery(txt_Search.Text, 3);
            CreateSearchResultTab(txt_Search.Text, results);
        }
        private async void CreateSearchResultTab(string queryString, List<VideoInformation> results)
        {
            TabPage page = new TabPage(string.Format("Search Results: {0}", queryString));
            SongList songList = new SongList();

            foreach(var video in results)
            {
                SongItem songItem = new SongItem(video.Url);
                songItem.SetTitleData(video.Title, video.Author, video.Duration);
                songItem.SetThumbnail(await video.GetThumbnail());
                songList.AddSongItem(songItem);
            }
            page.Controls.Add(songList);
            tabControl.TabPages.Add(page);
        }
        private async Task<ImageList> GenerateImageList(List<VideoInformation> results)
        {
            ImageList imageList = new ImageList();
            for(int i =0;i<results.Count;i++)
            {
                imageList.Images.Add(await results[i].GetThumbnail());
            }
            return imageList;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // get the tab
            if (tabControl.SelectedTab.Text != "Main")
                this.tabControl.TabPages.Remove(this.tabControl.SelectedTab);
        }
    }
}
