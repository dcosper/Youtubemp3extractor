﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YouTubeLib;
using YouTubeLib.Events;
using YouTubeLib.Options;
using System.IO;

namespace Youtube_MP3_Convertor.Controls
{
    public partial class SongItem : UserControl
    {
        public string Url
        {
            get;
            private set;
        }

        public SongItem(string url)
        {
            Url = url;
            InitializeComponent();
        }
        public void SetThumbnail(Image value)
        {
            //this.picBox_Thumbnail.
            this.picBox_Thumbnail.BackgroundImage = value;
        }
        public void SetTitleData(string title, string author, string duration)
        {
            this.rtf_SongTitle.AppendText(string.Format("Title:{0} Author:{1} Duration:{2}", title, author, duration));
        }

        private async void btn_Action_Click(object sender, EventArgs e)
        {
            btn_Action.Enabled = false;
            string savepath = SelectSavePath();
            bool complete = await SaveMusic(savepath);
            SetButtonText("Converted");

            if (!complete)
            {
                SetButtonText("Convert");
                btn_Action.Enabled = true;
            }
        }
        private async Task<bool> SaveMusic(string path)
        {
            using (YoutubeConvertor youtubeConvertor = new YoutubeConvertor())
            {
                try
                {
                    youtubeConvertor.OnDownloadError += DownLoadError;
                    youtubeConvertor.OnProgressUpdate += DownLoadUpdate;
                    youtubeConvertor.OnConversionError += ConversionError;
                    string fileName = await youtubeConvertor.DownLoadYoutubeLink(Url, path);
                    DownLoadUpdate(100);
                    string mp3PreName = string.Format("{0}.{1}", fileName.GetPathWithoutExtention(), ConvertableType.MP3.ToString().ToLower());

                    if (File.Exists(mp3PreName))
                    {
                        DialogResult exists = MessageBox.Show(string.Format("File {0} exists, overwrite it ?", mp3PreName), "Overwite exisiting file ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                        if (exists != DialogResult.Yes)
                            return false;
                    }
                    
                    string convertedFilename = await youtubeConvertor.TryMediaConvert(fileName, YouTubeLib.Options.ConvertableType.MP3);
                    DownLoadUpdate(100);
                    return true;
                }
                catch (Exception ex)
                {
                    ConversionError(ex);
                    return false;
                }
            }
        }
        public string SelectSavePath()
        {
            FolderBrowserDialog f = new FolderBrowserDialog();
            f.Description = "Select your MP3 directory.";
            if (!string.IsNullOrEmpty(Form1.Instance.LastMP3SavePath))
            {
                f.SelectedPath = Form1.Instance.LastMP3SavePath;
                return f.SelectedPath;
            }

            DialogResult r = f.ShowDialog();

            if (r == DialogResult.OK)
            {
                Form1.Instance.LastMP3SavePath = f.SelectedPath;
            }
            return f.SelectedPath;
        }
        private void SetButtonText(string value)
        {
            if (btn_Action.InvokeRequired)
            {
                btn_Action.Invoke((MethodInvoker)
                    delegate
                    {
                        btn_Action.Text = value;
                    });
            }
            else btn_Action.Text = value;
        }
        private void ConversionError(Exception ex)
        {
            throw new NotImplementedException();
        }

        private void DownLoadUpdate(int percentage)
        {
            SetButtonText(string.Format("{0}% Complete", percentage));
        }

        private void DownLoadError(Exception ex)
        {
            throw new NotImplementedException();
        }
    }
}
