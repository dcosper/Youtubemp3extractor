﻿namespace Youtube_MP3_Convertor.Controls
{
    partial class SongItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SongContainer = new System.Windows.Forms.Panel();
            this.btn_Action = new System.Windows.Forms.Button();
            this.rtf_SongTitle = new System.Windows.Forms.RichTextBox();
            this.picBox_Thumbnail = new System.Windows.Forms.PictureBox();
            this.SongContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox_Thumbnail)).BeginInit();
            this.SuspendLayout();
            // 
            // SongContainer
            // 
            this.SongContainer.AutoSize = true;
            this.SongContainer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SongContainer.Controls.Add(this.btn_Action);
            this.SongContainer.Controls.Add(this.rtf_SongTitle);
            this.SongContainer.Controls.Add(this.picBox_Thumbnail);
            this.SongContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SongContainer.Location = new System.Drawing.Point(0, 0);
            this.SongContainer.Name = "SongContainer";
            this.SongContainer.Size = new System.Drawing.Size(652, 106);
            this.SongContainer.TabIndex = 0;
            // 
            // btn_Action
            // 
            this.btn_Action.Location = new System.Drawing.Point(518, 38);
            this.btn_Action.Name = "btn_Action";
            this.btn_Action.Size = new System.Drawing.Size(127, 36);
            this.btn_Action.TabIndex = 5;
            this.btn_Action.Text = "Convert";
            this.btn_Action.UseVisualStyleBackColor = true;
            this.btn_Action.Click += new System.EventHandler(this.btn_Action_Click);
            // 
            // rtf_SongTitle
            // 
            this.rtf_SongTitle.Location = new System.Drawing.Point(220, 25);
            this.rtf_SongTitle.Name = "rtf_SongTitle";
            this.rtf_SongTitle.ReadOnly = true;
            this.rtf_SongTitle.Size = new System.Drawing.Size(292, 65);
            this.rtf_SongTitle.TabIndex = 4;
            this.rtf_SongTitle.Text = "";
            // 
            // picBox_Thumbnail
            // 
            this.picBox_Thumbnail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picBox_Thumbnail.Location = new System.Drawing.Point(0, 6);
            this.picBox_Thumbnail.Name = "picBox_Thumbnail";
            this.picBox_Thumbnail.Size = new System.Drawing.Size(214, 100);
            this.picBox_Thumbnail.TabIndex = 3;
            this.picBox_Thumbnail.TabStop = false;
            // 
            // SongItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SongContainer);
            this.Name = "SongItem";
            this.Size = new System.Drawing.Size(652, 106);
            this.SongContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBox_Thumbnail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel SongContainer;
        private System.Windows.Forms.Button btn_Action;
        private System.Windows.Forms.RichTextBox rtf_SongTitle;
        private System.Windows.Forms.PictureBox picBox_Thumbnail;
    }
}
