﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Youtube_MP3_Convertor.Controls
{
    public partial class SongList : UserControl
    {
        private List<SongItem> _songs;
        public List<SongItem> Songs
        {
            get { return _songs; }
        }
        public SongList()
        {
            InitializeComponent();
            _songs = new List<SongItem>();
        }
        public void AddSongItem(SongItem songItem)
        {
            songItem.Dock = DockStyle.Top;
            _songs.Add(songItem);
            this.panel1.Controls.Add(songItem);
        }
    }
}
